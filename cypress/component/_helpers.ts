export const the_middle_of_nowhere = [-16,-12];

export const getPictureDiff = (
  res: any
):number => 
  res.match 
    ? 0
    : res.reason === 'pixel-diff'
      ? res.diffPercentage
      : 100;
