import { setDefaultInterceptors } from '@/../cypress/support/interceptors';
import { saveTempScreenshot } from '@/../cypress/support/visual/saveTempScreenshot';
import MapboxGeogeometryRectangleBase from './scenarios/MapboxGeogeometryRectangle.base.vue';

import config from '../support/config';
import { getPictureDiff, the_middle_of_nowhere } from './_helpers';

describe('MapboxGeogeometry.Rectangle', () => {
  beforeEach(() => {
    setDefaultInterceptors();
  });

  it('successfully mounts', () => {
    cy.mount(MapboxGeogeometryRectangleBase as any, {
      props: {
        accessToken: Cypress.env('ACCESS_TOKEN'),
        center: the_middle_of_nowhere,
        width: 500,
        height: 400
      }
    });
  });

  it('will show a rectangle', () => {
    cy.mount(MapboxGeogeometryRectangleBase as any, {
      props: {
        accessToken: Cypress.env('ACCESS_TOKEN'),
        center: the_middle_of_nowhere,
        width: 800,
        height: 400
      }
    }).wait(config.defaultWaitTimes.geogeometryRectangle);

    const tempFile = saveTempScreenshot('canvas');

    cy.task('compare', {
      original: 'MapboxGeogeometry.Rectangle.base',
      compareTo: tempFile
    }).then((res:any) => {
      assert(getPictureDiff(res) < 1);
    });
  });

  it('will correctly rotate the rectangle', () => {
    cy.mount(MapboxGeogeometryRectangleBase as any, {
      props: {
        accessToken: Cypress.env('ACCESS_TOKEN'),
        center: the_middle_of_nowhere,
        width: 800,
        height: 400,
        rotationDeg: 15
      }
    }).wait(config.defaultWaitTimes.geogeometryRectangle);

    const tempFile = saveTempScreenshot('canvas');

    cy.task('compare', {
      original: 'MapboxGeogeometry.Rectangle.base-rotated',
      compareTo: tempFile
    }).then((res:any) => {
      assert(getPictureDiff(res) < 1);
    });
  });
});